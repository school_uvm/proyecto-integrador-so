package uvm.so;

public final class Msg {

	public static final String MSG_ERR_CONTACTOS_VACIO = "No existe ningún contacto aún.";
	/**
	 * Mensaje de error para el texto vació
	 */
	public static final String MSG_ERR_TEXTO_VACIO = "El contenido no debe estar vació.";
	/**
	 * Mensaje de error para una opción inválida en una colección
	 */
	public static final String MSG_ERR_LISTA = "El elemento no es valido.";
	/**
	 * Mensaje cuando la acción ha sido cancelada
	 */
	public static final String MSG_ERR_CANCEL = "Acción cancelada";

	/**
	 * This class cannot be instantiated
	 */
	private Msg() {
	}

}
