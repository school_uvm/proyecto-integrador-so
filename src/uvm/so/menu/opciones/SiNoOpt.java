package uvm.so.menu.opciones;

/**
 * Tipo enumerado para verificar si cierta acción se quiere realizar
 */
public enum SiNoOpt {
	/**
	 * Verdadero
	 */
	SI,
	/**
	 * Falso
	 */
	NO
}
