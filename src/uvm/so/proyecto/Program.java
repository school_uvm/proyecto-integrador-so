package uvm.so.proyecto;

import uvm.so.error.InfoException;
import uvm.so.menu.Menu;
import uvm.so.proyecto.consola.MenuPrincipal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Program {

	/**
	 * Current object logging system
	 */
	private static final Logger LOG = Logger.getLogger(Program.class.getName());

	/**
	 * Método principal del programa
	 *
	 * @param args argumentos del programa
	 */
	@SuppressWarnings("ConstantValue")
	public static void main(String[] args) {
		// Creamos flujos de entrada para la manipulación del teclado.
		// Para esto se utilizan los elementos [Autocloseable] que son
		// interfaces que cierran automáticamente un flujo (Stream) sin
		// necesidad de cerrarlos manualmente
		try (var stream = new InputStreamReader(System.in);
			 var reader = new BufferedReader(stream)) {
			// Generamos una nueva instancia del menu que se encargará de
			// manipular la información
			Menu menuPrincipal = new MenuPrincipal(reader);
			// Mostramos el menu principal
			menuPrincipal.mostrarMenu(reader);
		} catch (Exception e) {
			if (e instanceof InfoException) {
				LOG.log(Level.SEVERE, e.getMessage());
			} else {
				e.printStackTrace();
			}
		}
	}

}
