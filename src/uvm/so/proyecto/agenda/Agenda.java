package uvm.so.proyecto.agenda;

import uvm.so.error.InfoException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public final class Agenda {

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * La instancia del objeto al que se va a acceder.
	 * Esta propiedad debe ser de forma estática y privada debido a que
	 * solo es necesario crear una instancia para toda la ejecución del programa
	 * y no crear muchas instancias de la misma.
	 */
	private static Agenda INSTANCE;

	/**
	 * Cantidad de elementos en la agenda
	 */
	private int cantidad = 0;

	/**
	 * Los contactos que tiene la agenda
	 */
	private Contacto[] contactos;

	/* ---------------------------------------------------------------
	 * Constructores
	 * ------------------------------------------------------------ */

	/**
	 * Esta clase no puede crear instancias de manera común. Por eso el constructor
	 * es privado
	 * <p>
	 * Utilizado de para utilizar el modelo Singleton
	 */
	private Agenda() {
		// Iniciamos las propiedades
		contactos = new Contacto[cantidad];
	}

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Accede a la instancia global que existe dentro de la propia clase
	 *
	 * @return la instancia del objeto
	 */
	public static Agenda getInstance() {
		// Primero verificamos que la agenda no exista
		if (INSTANCE == null) {
			// Creamos la instancia del objeto
			INSTANCE = new Agenda();
		}
		// Devolvemos el objeto creado. Solo si es necesario
		// crearse, pero no es necesario pasar por esto todas las veces
		return INSTANCE;
	}

	/**
	 * Devuelve la cantidad de contactos registrados
	 *
	 * @return cantidad de contactos
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * Devuelve todos los contactos
	 *
	 * @return todos los contactos
	 */
	public List<Contacto> getContactos() {
		return List.of(contactos);
	}

	/**
	 * Agrega un nuevo contacto a la agenda
	 *
	 * @param newContacto el contacto que se desea agregar
	 * @throws Exception error si el contacto ya existe o no es valido
	 */
	public void agregar(final Contacto newContacto) throws Exception {
		// Verificamos que el contacto sea válido
		verificarContacto(newContacto);
		limpiarContacto(newContacto);
		// Verificamos si el contacto ya existe
		if (existeContacto(newContacto))
			throw new InfoException("El contacto \"%s\" ya existe", newContacto);
		// Generamos un nuevo array con todos los objetos
		Contacto[] contactosTmp = new Contacto[++cantidad];
		System.arraycopy(contactos, 0, contactosTmp, 0, contactos.length);
		// El último elemento siempre será el objeto nuevo
		contactosTmp[contactos.length] = newContacto;
		// Remplazamos el array temporal por la propiedad del objeto
		contactos = contactosTmp;
		// Ordenamos los contactos
		ordenar();
	}

	/**
	 * Agrega un nuevo contacto a la agenda
	 *
	 * @param nombre   el nombre del contacto
	 * @param correo   el correo del contacto
	 * @param telefono el teléfono del contacto
	 * @throws Exception error si el contacto ya existe o no es valido
	 */
	public void agregar(String nombre, String correo, String telefono) throws Exception {
		agregar(new Contacto(nombre, correo, telefono));
	}

	/**
	 * Agrega un nuevo contacto a la agenda
	 *
	 * @param nombre   el nombre del contacto
	 * @param telefono el teléfono del contacto
	 * @throws Exception error si el contacto ya existe o no es valido
	 */
	public void agregar(String nombre, String telefono) throws Exception {
		agregar(nombre, "", telefono);
	}

	/**
	 * Elimina el contacto de la agenda
	 *
	 * @param contacto el nombre o el número telefónico del contacto
	 * @throws Exception error si el contacto no existe o no se pudo realizar
	 *                   la acción
	 */
	public void eliminarContacto(String contacto) throws Exception {
		// Buscamos el contacto y lo guardamos de forma temporal
		Optional<Contacto> contactoObj = buscarContacto(contacto);

		// Verificamos que el contacto exista
		if (contactoObj.isEmpty())
			throw new InfoException("El contacto \"%s\" no existe o la información es incorrecta",
									contacto);

		// Buscamos el índice del contacto
		int indice = buscarIndiceContacto(contactoObj.get());
		if (indice == -1)
			throw new InfoException("El contacto \"%s\" no existe",
									contactoObj.get());

		// Generamos un nuevo array con las dimensiones nuevas
		Contacto[] contactosTmp = new Contacto[--cantidad];

		// Copiamos la información de los contactos en el nuevo array,
		// pero primero iniciado por la copia de la izquierda
		System.arraycopy(contactos, 0, contactosTmp, 0, indice);
		// La segunda copia debe ser verificada, solo si el índice es menor al tamaño del
		// nuevo array temporal
		if (indice < contactosTmp.length) {
			// Es aquí cuando copiamos la parte derecha del array
			System.arraycopy(contactos, indice + 1, contactosTmp, indice, contactosTmp.length - indice);
		}
		// Cambiamos la propiedad de los contactos
		// por el array temporal
		contactos = contactosTmp;
	}

	/**
	 * Modifica la información del contacto
	 *
	 * @param contacto    el contacto que desea modificar
	 * @param informacion la nueva información que se quiere modificar
	 * @throws Exception error si el contacto no existe o no se pudo realizar la acción
	 */
	public void modificarContacto(String contacto, Contacto informacion) throws Exception {
		// Buscamos el contacto que se señala
		Optional<Contacto> contactoOpt = buscarContacto(contacto);
		// Verificamos que el contacto exista
		if (contactoOpt.isEmpty())
			throw new InfoException("El contacto \"%s\" no existe o la información es incorrecta",
									contacto);
		// En esta ocasión es necesario eliminar al contacto antiguo para modificar dicha información
		eliminarContacto(contacto);
		// Realizamos los cambios del contacto nuevo
		Contacto contactoObj = contactoOpt.get();
		Contacto contactoFinal = new Contacto(
			informacion.getNombre().isBlank() ? contactoObj.getNombre() : informacion.getNombre(),
			informacion.getEmail().isBlank() ? contactoObj.getEmail() : informacion.getEmail(),
			informacion.getTelefono().isBlank() ? contactoObj.getTelefono() : informacion.getTelefono()
		);

		// Tratamos de realizar la acción de agregar el contacto
		try {
			agregar(contactoFinal);
		} catch (Exception e) {
			// En caso de ocurrir cualquier error y no perder información
			// agregamos el contacto eliminado
			agregar(contactoObj);
			throw new InfoException("La información ingresada ya se encuentra utilizada por otro contacto");
		}
	}

	/**
	 * Busca el contacto ya sea por nombre o teléfono
	 *
	 * @param contacto el nombre o teléfono del contacto
	 * @return devuelve el contacto buscado o {@link Optional#empty()} si no existe
	 */
	public Optional<Contacto> buscarContacto(String contacto) {
		return Arrays.stream(contactos)
			.filter(it -> it.getNombre().equalsIgnoreCase(contacto) ||
				it.getTelefono().equalsIgnoreCase(limpiarTelefono(contacto)))
			.findFirst();
	}

	/**
	 * Verifica si el contacto existe
	 *
	 * @param contacto el contacto que desea encontrar
	 * @param telefono el teléfono del contacto
	 * @return devuelve {@code true} si el contacto existe o {@code false} de otra forma
	 */
	public boolean existeContacto(String contacto, String telefono) {
		return Arrays.stream(contactos)
			.anyMatch(it -> it.getNombre().equalsIgnoreCase(contacto) || it.getTelefono().equalsIgnoreCase(telefono));
	}

	/**
	 * Verifica si el contacto existe
	 *
	 * @param contacto el contacto que desea encontrar
	 * @return devuelve {@code true} si el contacto existe o {@code false} de otra forma
	 */
	public boolean existeContacto(final Contacto contacto) {
		return existeContacto(contacto.getNombre(), contacto.getTelefono());
	}

	/**
	 * Ordena los contactos por orden alfabético
	 */
	public void ordenar() {
		// Verificamos que la agenda tenga más de 2 contactos
		if (cantidad < 2) return;
		// Los flujos ya cuentan con funcionalidad para ordenar los datos
		// por esa razón es más sencillo de utilizar, además de que cuentan
		// con muchas funcionalidades extras.
		contactos = Arrays.stream(contactos)
			.sorted(Comparator.comparing(Contacto::getNombre))
			.toArray(Contacto[]::new);
	}

	/* ---------------------------------------------------------------
	 * Métodos privados
	 * ------------------------------------------------------------ */

	/**
	 * Elimina todos los contactos de la agenda
	 */
	public void vaciar() {
		// Cuando se reescriben los valores en java estos directamente
		// pasan a ser parte del recolector de basura y por consiguiente,
		// toda la información es eliminada
		cantidad = 0;
		contactos = new Contacto[cantidad];
	}

	/**
	 * Verifica el contacto que se intenta modificar
	 *
	 * @param contacto el contacto que se va a analizar
	 * @throws Exception error al verificar el contacto
	 */
	private void verificarContacto(Contacto contacto) throws Exception {
		// Verificamos si el correo está correcto
		if (!contacto.getEmail().isBlank() && !Contacto.esCorreoValido(contacto.getEmail()))
			throw new InfoException("El correo ingresado no es valido. Los formatos correctos son: %s",
									Arrays.toString(Contacto.EMAIL_VALID_FORMATS));

		// Verificamos si el teléfono es correcto
		if (!contacto.getTelefono().isBlank() && !Contacto.esTelefonoValido(contacto.getTelefono()))
			throw new InfoException("El teléfono ingresado no es valido. Los formatos validos son: %s",
									Arrays.toString(Contacto.PHONE_VALID_FORMATS));
	}

	/**
	 * Limpiamos el contenido del teléfono
	 *
	 * @param contacto el contacto que se desea modificar
	 */
	private void limpiarContacto(Contacto contacto) {
		// Limpiamos el teléfono y lo pasamos a un formato más común
		contacto.setTelefono(limpiarTelefono(contacto.getTelefono()));
		// Para el correo, simplemente eliminamos los espacios de las orillas
		contacto.setEmail(contacto.getEmail().trim());
	}

	/**
	 * Realizamos la limpieza del teléfono para pasarlo a un formato
	 * más común y prevenir errores a la hora de comprobar números telefónicos
	 *
	 * @param telefono el teléfono que se desea limpiar
	 * @return devuelve el teléfono ya con el formato deseado
	 */
	private String limpiarTelefono(String telefono) {
		// Verificamos que el telefono sea correcto
		if (!Contacto.esTelefonoValido(telefono))
			return telefono;

		// Eliminamos todos los caracteres especiales para que el teléfono
		// sea consistente en todos los teléfonos. Además, también eliminamos todos
		// los espacios intermedios para prevenir errores
		telefono = telefono.replaceAll("[\\-|()\\s]*", "");

		// Definimos un formato standard y cambiamos el teléfono del objeto
		String telefonoFinal = telefono.substring(0, 3); // los primeros 3 dígitos
		telefonoFinal += "-" + telefono.substring(3, 6); // los segundos 3 dígitos
		telefonoFinal += "-" + telefono.substring(6, 10); // los últimos 4 dígitos

		return telefonoFinal.trim();
	}

	/* ---------------------------------------------------------------
	 * Métodos estáticos
	 * ------------------------------------------------------------ */

	/**
	 * Buscamos en todos los contactos el índice en el que se encuentra
	 * el contacto seleccionado
	 *
	 * @param contacto el contacto que se desea buscar
	 * @return el índice del contacto o {@code -1} si el contacto no existe
	 */
	private int buscarIndiceContacto(Contacto contacto) {
		// Guardamos el resultado para manipularlo después
		int indice = -1;

		// Iteramos todos los elementos
		for (int i = 0; i < cantidad; ++i) {
			if (contactos[i].equals(contacto)) {
				indice = i;
				break;
			}
		}

		return indice;
	}

}
