package uvm.so.proyecto.agenda;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Contacto {

	/* ---------------------------------------------------------------
	 * Expresiones regulares
	 * ------------------------------------------------------------ */

	/**
	 * Todos los formatos válidos para el número telefónico
	 */
	public static final String[] PHONE_VALID_FORMATS = new String[]{
		"0000000000",
		"000-000-0000",
		"(000)000-0000",
		"(000)0000000"
	};
	/**
	 * Todos los formatos válidos de correos electrónicos
	 */
	public static final String[] EMAIL_VALID_FORMATS = new String[]{
		"example@domain.xxx"
	};
	/**
	 * Expression regular para verificar correos electrónicos
	 */
	private static final Pattern EMAIL_PATTERN = Pattern
		.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	/**
	 * Expression regular para verificar números telefónicos
	 */
	private static final Pattern PHONE_PATTERN = Pattern
		.compile("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\s?\\d{3}-?\\d{4}");

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */
	/**
	 * Nombre del contacto
	 */
	private String nombre;

	/**
	 * El correo electrónico del contacto
	 */
	private String email;

	/**
	 * El teléfono del contacto
	 */
	private String telefono;

	/* ---------------------------------------------------------------
	 * Constructores
	 * ------------------------------------------------------------ */

	/**
	 * Constructor con las propiedades de la clase
	 *
	 * @param nombre   nombre del contacto
	 * @param email    correo del contacto
	 * @param telefono teléfono del contacto
	 */
	public Contacto(String nombre, String email, String telefono) {
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
	}

	/**
	 * Constructor sin elementos
	 */
	public Contacto() {
		this("", "", "");
	}

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Verifica que el correo sea válido. Esto se realiza mediante expresiones regulares
	 * dando como resultado una implementación más limpia y fácil de depurar
	 *
	 * @param correo el correo que se desea verificar
	 * @return devuelve {@code true} si el correo es válido o {@code false} de otra forma
	 */
	public static boolean esCorreoValido(final String correo) {
		Matcher matcher = EMAIL_PATTERN.matcher(correo);
		return matcher.matches();
	}

	/**
	 * Verifica que el telefono sea válido.
	 *
	 * @param telefono el teléfono que se desea verificar
	 * @return devuelve {@code true} si el correo es válido o {@code false} de otra forma
	 */
	public static boolean esTelefonoValido(final String telefono) {
		Matcher matcher = PHONE_PATTERN.matcher(telefono);
		return matcher.matches();
	}

	/**
	 * Devuelve el nombre del contacto
	 *
	 * @return el nombre del contacto
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Cambia el nombre del contacto
	 *
	 * @param nombre el nuevo nombre del contacto
	 */
	void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve el correo electrónico del contacto
	 *
	 * @return el correo electrónico del contacto
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Cambia del correo electrónico del contacto
	 *
	 * @param email el nuevo correo electrónico
	 */
	void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Devuelve el teléfono del contacto
	 *
	 * @return el teléfono del contacto
	 */
	public String getTelefono() {
		return telefono;
	}

	/* ---------------------------------------------------------------
	 * Métodos estáticos
	 * ------------------------------------------------------------ */

	/**
	 * Cambia el teléfono del contacto
	 *
	 * @param telefono el nuevo teléfono
	 */
	void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * Representación del objeto a texto
	 *
	 * @return representación del objeto a texto
	 */
	@Override
	public String toString() {
		// Verificamos si el contacto tiene correo electronico
		if (getEmail().isBlank()) {
			return String.format("%s - %s", getNombre(), getTelefono());
		} else {
			return String.format("%s (%s) - %s", getNombre(), getEmail(), getTelefono());
		}
	}

}
