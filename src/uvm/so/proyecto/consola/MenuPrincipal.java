package uvm.so.proyecto.consola;

import uvm.so.error.InfoException;
import uvm.so.menu.Menu;
import uvm.so.menu.MenuItem;
import uvm.so.menu.opciones.SiNoOpt;
import uvm.so.proyecto.agenda.Agenda;
import uvm.so.proyecto.agenda.Contacto;

import java.io.BufferedReader;
import java.util.List;
import java.util.Optional;

import static uvm.so.Msg.*;
import static uvm.so.utilidades.EntradaUtils.*;

public class MenuPrincipal extends Menu {

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * La entrada del teclado
	 */
	private final BufferedReader reader;

	/**
	 * Todos los elementos del menu
	 */
	private final List<MenuItem> elementos = List.of(
		new MenuItem("Agregar contacto", this::agregarContacto),
		new MenuItem("Buscar contacto", this::buscarContacto),
		new MenuItem("Eliminar contacto", this::eliminarContacto),
		new MenuItem("Modificar contacto", this::modificarContacto),
		new MenuItem("Mostrar contactos", this::mostrarContactos));

	/* ---------------------------------------------------------------
	 * Constructores
	 * ------------------------------------------------------------ */

	/**
	 * Constructor por defecto
	 *
	 * @param entrada la entrada del teclado
	 */
	public MenuPrincipal(BufferedReader entrada) {
		super("Agenda");
		reader = entrada;
		inicializarMenu();
	}

	/* ---------------------------------------------------------------
	 * Métodos acciones
	 * ------------------------------------------------------------ */

	/**
	 * Agrega un nuevo contacto a la agenda
	 *
	 * @throws Exception error si el contacto ya existe o
	 *                   algún error de lectura de teclado
	 */
	private void agregarContacto() throws Exception {
		// Creamos la nueva instancia del contacto
		Contacto resultado = new Contacto(
			preguntarTexto("Ingrese el nombre del contacto", MSG_ERR_TEXTO_VACIO, reader),
			preguntarTexto("Ingrese el email", MSG_ERR_TEXTO_VACIO, true, reader),
			preguntarTexto("Ingrese el numero", MSG_ERR_TEXTO_VACIO, reader)
		);

		// Agregamos el contacto a la agenda
		Agenda.getInstance().agregar(resultado);
	}

	/**
	 * Busca un contacto mediante una entrada de datos
	 *
	 * @throws Exception error si el contacto no existe o
	 *                   algún error de lectura de teclado
	 */
	private void buscarContacto() throws Exception {
		// Verificamos que exista al menos un contacto
		if (Agenda.getInstance().getCantidad() == 0)
			throw new InfoException(MSG_ERR_CONTACTOS_VACIO);
		// Buscamos al contacto
		String contactoTxt = preguntarTexto(
			"Ingresa el nombre o el teléfono del contacto",
			"El contenido no debe de estar vació",
			reader);
		Optional<Contacto> contacto = Agenda.getInstance()
			.buscarContacto(contactoTxt);

		// Verificamos si el objeto contiene al contacto
		if (contacto.isEmpty())
			throw new InfoException("El contacto \"%s\" no existe.", contactoTxt);

		// Mostramos la información del contacto
		System.out.println(contacto.get());
	}

	/**
	 * Elimina un contacto de la agenda
	 *
	 * @throws Exception error si el contacto no existe o
	 *                   algún error de lectura de teclado
	 */
	private void eliminarContacto() throws Exception {
		// Verificamos que exista al menos un contacto
		if (Agenda.getInstance().getCantidad() == 0)
			throw new InfoException(MSG_ERR_CONTACTOS_VACIO);
		// Preguntamos por el usuario que se quiere eliminar
		Contacto contacto = preguntarCollection(
			"Seleccione al contacto que quiere eliminar",
			MSG_ERR_LISTA,
			Agenda.getInstance().getContactos(),
			reader);
		// Le preguntamos al usuario si realmente quiere
		// realizar dicha acción
		SiNoOpt confirmar = preguntarTipoEnum(
			"Realmente quiere hacer realizar esta acción (la información se perderá)",
			MSG_ERR_LISTA,
			SiNoOpt.class,
			reader);

		if (confirmar != SiNoOpt.SI)
			throw new InfoException(MSG_ERR_CANCEL);

		// Eliminamos el contacto
		Agenda.getInstance()
			.eliminarContacto(contacto.getNombre());
	}

	/**
	 * Modifica la información de un contacto
	 *
	 * @throws Exception error al modificar el contacto o
	 *                   algún error de lectura de teclado
	 */
	private void modificarContacto() throws Exception {
		// Verificamos que exista al menos un contacto
		if (Agenda.getInstance().getCantidad() == 0)
			throw new InfoException(MSG_ERR_CONTACTOS_VACIO);
		// Preguntamos por el usuario que se quiere eliminar
		Contacto contacto = preguntarCollection(
			"Seleccione al contacto que quiere modificar",
			MSG_ERR_LISTA,
			Agenda.getInstance().getContactos(),
			reader);

		// Creamos la nueva instancia del contacto
		Contacto nuevaInformacion = new Contacto(
			preguntarTexto("Ingrese el nuevo nombre (dejar vació si no quiere cambiarlo)", MSG_ERR_TEXTO_VACIO, reader),
			preguntarTexto("Ingrese el nuevo email (dejar vació si no quiere cambiarlo)", MSG_ERR_TEXTO_VACIO, true, reader),
			preguntarTexto("Ingrese el nuevo numero (dejar vació si no quiere cambiarlo)", MSG_ERR_TEXTO_VACIO, reader)
		);

		// Tratamos de cambiar la información del contacto
		Agenda.getInstance()
			.modificarContacto(contacto.getNombre(), nuevaInformacion);
	}

	/**
	 * Mostramos todos los contactos de la agenda
	 *
	 * @throws Exception error si no existe ningún contacto
	 */
	private void mostrarContactos() throws Exception {
		// Verificamos que exista al menos un contacto
		if (Agenda.getInstance().getCantidad() == 0)
			throw new InfoException(MSG_ERR_CONTACTOS_VACIO);
		// Mostramos todos los contactos
		for (Contacto contacto : Agenda.getInstance().getContactos()) {
			System.out.println(contacto);
		}
	}

	/* ---------------------------------------------------------------
	 * Métodos internos
	 * ------------------------------------------------------------ */

	/**
	 * Iniciamos todos los elementos del menu
	 */
	private void inicializarMenu() {
		// Creamos los elementos del menu
		for (MenuItem elemento : elementos) {
			agregarElemento(elemento);
		}
	}

}
